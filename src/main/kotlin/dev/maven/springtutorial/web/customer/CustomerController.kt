package dev.maven.springtutorial.web.customer

import dev.maven.springtutorial.domain.customer.CustomerService
import dev.maven.springtutorial.domain.exception.DuplicateException
import dev.maven.springtutorial.web.exception.DuplicateHttpException
import dev.maven.springtutorial.web.customer.request.AddCustomerRequestBody
import dev.maven.springtutorial.web.customer.request.CustomerAssembler
import dev.maven.springtutorial.web.customer.response.CustomerResponseEntity
import dev.maven.springtutorial.web.customer.response.CustomerResponseMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/customer")
class CustomerController(
    @Autowired private val customerService: CustomerService,
    @Autowired private val customerResponseMapper: CustomerResponseMapper,
    @Autowired private val customerAssembler: CustomerAssembler
) {
    @GetMapping
    fun getAllCustomers(): List<CustomerResponseEntity> {
        val customers = customerService.getAllCustomers()
        val customersResponseEntities = customerResponseMapper.mapCustomersToResponseEntities(customers)
        return customersResponseEntities
    }
    @PostMapping
    fun addCustomer(@RequestBody addCustomerBody: AddCustomerRequestBody) {
        val customer = customerAssembler.createCustomerFrom(addCustomerBody)

        try {
            customerService.addNewCustomer(customer)
        } catch (exception: DuplicateException) {
            throw DuplicateHttpException(exception.message)
        }
    }

    @DeleteMapping("{id}")
    fun deleteCustomer(@PathVariable("id") customerId: Int) {
        customerService.deleteCustomer(customerId)
    }
}