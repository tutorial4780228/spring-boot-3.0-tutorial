package dev.maven.springtutorial.web.customer.response

class CustomerResponseEntity (
    val id: Int,
    val full_name: String,
    val email_address: String,
    val age: Int
)