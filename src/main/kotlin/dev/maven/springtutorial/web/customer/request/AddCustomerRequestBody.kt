package dev.maven.springtutorial.web.customer.request

class AddCustomerRequestBody (
    val full_name: String,
    val email_address: String,
    val age: Int
)