package dev.maven.springtutorial.web.customer.response

import dev.maven.springtutorial.domain.customer.Customer
import org.springframework.stereotype.Service

@Service
class CustomerResponseMapper {
    fun mapCustomersToResponseEntities(customers: List<Customer>): List<CustomerResponseEntity> {
        return customers.map {
            customer -> mapCustomerToResponseEntity(customer)
        }
    }

    fun mapCustomerToResponseEntity(customer: Customer): CustomerResponseEntity {
        return CustomerResponseEntity(
            customer.id,
            customer.name,
            customer.email,
            customer.age
        )
    }
}