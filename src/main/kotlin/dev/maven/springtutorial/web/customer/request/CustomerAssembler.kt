package dev.maven.springtutorial.web.customer.request

import dev.maven.springtutorial.domain.customer.Customer
import org.springframework.stereotype.Service

@Service
class CustomerAssembler {
    fun createCustomerFrom(addCustomerRequestBody: AddCustomerRequestBody): Customer {
        return Customer(
            name = addCustomerRequestBody.full_name,
            email = addCustomerRequestBody.email_address,
            age = addCustomerRequestBody.age
        )
    }
}