package dev.maven.springtutorial.web.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
class DuplicateHttpException(override val message: String?) : RuntimeException(message)