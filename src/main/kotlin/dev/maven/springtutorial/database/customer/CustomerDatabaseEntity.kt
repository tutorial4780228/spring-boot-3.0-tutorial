package dev.maven.springtutorial.database.customer

import jakarta.persistence.*
import java.util.Objects

@Entity(name = "customer")
class CustomerDatabaseEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int = 0,
    val name: String = "",
    val email: String = "",
    val age: Int = 0
) {
    override fun toString(): String {
        return "#$id $name ($email) - $age y.o."
    }

    override fun hashCode(): Int {
        return Objects.hash(id, name, email, age)
    }

    override fun equals(other: Any?): Boolean {
        when {
            (this === other) -> return true
            (javaClass != other?.javaClass) -> return false
        }
        other as CustomerDatabaseEntity

        return when {
            (id != other.id) -> false
            (name != other.name) -> false
            (email != other.email) -> false
            (age != other.age) -> false
            else -> true
        }
    }
}