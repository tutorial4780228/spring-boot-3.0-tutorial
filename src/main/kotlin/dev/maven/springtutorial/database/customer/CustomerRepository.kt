package dev.maven.springtutorial.database.customer

import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

interface CustomerRepository : JpaRepository<CustomerDatabaseEntity, Int> {
    fun findFirstByNameAndEmail(name: String, email: String): Optional<CustomerDatabaseEntity>;
}