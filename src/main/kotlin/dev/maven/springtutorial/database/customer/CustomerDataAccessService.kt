package dev.maven.springtutorial.database.customer

import dev.maven.springtutorial.domain.customer.Customer
import dev.maven.springtutorial.domain.customer.CustomerGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class CustomerDataAccessService(
    @Autowired private val customerRepository: CustomerRepository
) : CustomerGateway {
    override fun getAllCustomers(): List<Customer> {
        val customerDatabaseEntities = customerRepository.findAll()
        val customers = mapDatabaseEntitiesToCustomers(customerDatabaseEntities)
        return customers
    }

    override fun customerWithAttributesExists(name: String, email: String): Boolean {
        val existingCustomer = customerRepository.findFirstByNameAndEmail(name, email)
        return existingCustomer.isPresent
    }

    override fun getCustomerById(id: Int): Optional<Customer> {
        val customerDatabaseEntity = customerRepository.findById(id)
        if (customerDatabaseEntity.isEmpty) {
            return Optional.empty()
        }

        val customer = mapDatabaseEntityToCustomer(customerDatabaseEntity.get())
        return Optional.of(customer)
    }

    override fun addNewCustomer(customer: Customer) {
        val customerDatabaseEntity = mapCustomerToDatabaseEntity(customer)
        customerRepository.save(customerDatabaseEntity)
    }

    override fun deleteCustomerWithId(customerId: Int) {
        customerRepository.deleteById(customerId)
    }

    private fun mapCustomerToDatabaseEntity(customer: Customer): CustomerDatabaseEntity {
        return CustomerDatabaseEntity(
            name = customer.name,
            email = customer.email,
            age = customer.age
        )
    }

    private fun mapDatabaseEntitiesToCustomers(customerDatabaseEntities: List<CustomerDatabaseEntity>): List<Customer> {
        return customerDatabaseEntities.map {
            customer -> mapDatabaseEntityToCustomer(customer)
        }
    }

    private fun mapDatabaseEntityToCustomer(customerDatabaseEntity: CustomerDatabaseEntity): Customer {
        return Customer(
            customerDatabaseEntity.id,
            customerDatabaseEntity.name,
            customerDatabaseEntity.email,
            customerDatabaseEntity.age
        )
    }
}