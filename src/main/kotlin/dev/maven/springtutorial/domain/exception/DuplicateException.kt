package dev.maven.springtutorial.domain.exception

class DuplicateException(override val message: String?): RuntimeException(message)