package dev.maven.springtutorial.domain.customer

import java.util.Optional

interface CustomerGateway {
    fun getAllCustomers(): List<Customer>
    fun getCustomerById(id: Int): Optional<Customer>
    fun customerWithAttributesExists(name: String, email: String): Boolean
    fun addNewCustomer(customer: Customer)
    fun deleteCustomerWithId(customerId: Int)
}