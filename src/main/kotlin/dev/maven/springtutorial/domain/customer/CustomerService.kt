package dev.maven.springtutorial.domain.customer

import dev.maven.springtutorial.domain.exception.DuplicateException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerService(
    @Autowired private val customerGateway: CustomerGateway
) {
    fun getAllCustomers(): List<Customer> {
        return customerGateway.getAllCustomers()
    }

    fun addNewCustomer(newCustomer: Customer) {
        val customerAlreadyExists = customerGateway.customerWithAttributesExists(newCustomer.name, newCustomer.email)
        if (customerAlreadyExists) {
            throw DuplicateException("Customer ${newCustomer.name} (${newCustomer.email}) already exists")
        }
        customerGateway.addNewCustomer(newCustomer);
    }

    fun deleteCustomer(customerId: Int) {
        customerGateway.deleteCustomerWithId(customerId)
    }
}