package dev.maven.springtutorial.domain.customer

class Customer (
     val id: Int = 0,
     val name: String,
     val email: String,
     val age: Int
)